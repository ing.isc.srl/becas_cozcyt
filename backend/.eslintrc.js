module.exports = {
  env: {
    node: true,
    es6: true,
  },
  extends: ['prettier'],
  plugins: ['prettier'],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  parser: 'babel-eslint',
  rules: {
    strict: 0,
    'no-await-in-loop': 1,
    'no-duplicate-case': 1,
    'no-extra-parens': 0,
    'arrow-body-style': 1,
    'no-nested-ternary': 0,
    curly: 0,
    'default-case': 1,
    'no-case-declarations': 0,
    'no-constructor-return': 1,
    'no-empty-function': 1,
    'no-empty-pattern': 1,
  },
};
