import { Router } from 'express';

import { authMiddleware } from '../middlewares';
import { socioeconomicPDF } from './docs/PDF';
import { StudentServices } from '../../services';
import { calculateIncomes } from '../../utils';

const pdfRoute = Router();

pdfRoute.post('/', authMiddleware, async (req, res) => {
  const { curp } = req.decoded;
  const { family, sueldoMensual } = req.body;
  const document_path = `uploads/${curp}/estudioSocioeconomico.pdf`;
  const economicIncomes = calculateIncomes(sueldoMensual, family);

  const {
    personalData: { name, fstSurname, sndSurname },
  } = await StudentServices.updateDataOfStudent(curp, document_path, economicIncomes);
  const student_name = `${name} ${fstSurname} ${sndSurname}`;

  await socioeconomicPDF(req.body, student_name, document_path);
  res.send({ message: 'PDF created!' });
});

pdfRoute.get('/obtain', authMiddleware, async (req, res) => {
  const { curp } = req.decoded;
  res.download(`uploads/${curp}/estudioSocioeconomico.pdf`);
});

export { pdfRoute };
