import { Router } from 'express';

import { authMiddleware, upload } from '../middlewares';
import { StudentServices } from '../../services';
import { handleFilesData } from '../../utils';

const filesRoutes = Router();

filesRoutes.post('/new', [authMiddleware, upload.array('files', 10)], async (req, res) => {
  const { curp } = req.decoded;
  const files = req.files;
  const documents = handleFilesData(files);
  await StudentServices.updateDocumentsData(curp, documents);
  res.send({ message: 'Ya es toda goe' });
});

filesRoutes.post('/renovation', [authMiddleware, upload.array('files', 5)], async (req, res) => {
  const { curp } = req.decoded;
  const files = req.files;
  const documents = handleFilesData(files);
  await StudentServices.updateDocumentsData(curp, documents);
  res.send({ message: 'Ya es toda goe' });
});

filesRoutes.post('/single', [authMiddleware, upload.single('file')], async (req, res) => {
  res.send({ message: 'Pos ya checale' });
});

export { filesRoutes };
