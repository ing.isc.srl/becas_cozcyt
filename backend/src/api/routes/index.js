import { Router } from 'express';

import { completeRegistry } from './completeRegistry';
import { pdfRoute } from './socioeconomicPDF';
import { filesRoutes } from './uploadFiles';
import { getFile } from './getFile';
import { getXLSX } from './getXLSX';
import { getZip } from './getZip';

const router = Router();

router.use('/api/socioeconomicPDF', pdfRoute);
router.use('/api/uploadFiles', filesRoutes);
router.use('/api/getFile', getFile);
router.use('/api/completeRegistry', completeRegistry);
router.use('/api/getXLSX', getXLSX);
router.use('/api/getZip', getZip);

export { router };
