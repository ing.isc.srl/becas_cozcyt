import { Router } from 'express';

import { ScholarshipRequestsServices as Requests } from '../../services';
import { authMiddleware } from '../middlewares';
import { createXLSX } from './docs/XLSX';

const getXLSX = Router();

getXLSX.get('/', authMiddleware, async (req, res) => {
  const { userType: isAdmin } = req.decoded;
  if (!isAdmin) {
    const { states, students, institutes, points } = await Requests.getDataForDocument();
    createXLSX(students, institutes, points, states, res);
  } else res.status(500).send('Nel');
});

export { getXLSX };
