import xl from 'excel4node';

const headers = [
  'Estatus',
  'Estatus Beca',
  'Folio',
  'Nombre',
  'Ap Paterno',
  'Ap Materno',
  'Sexo',
  'Punt Sexo',
  'Edad',
  'CURP',
  'Calle y Num',
  'CP',
  'Colonia',
  'Municipio',
  'Punt Municipio',
  'Estado',
  'Tel Casa',
  'Tel Cel',
  'Correo',
  'Institución',
  'Punt Institución',
  'Carrera',
  'Punt Carrera',
  'Periodo',
  'Punt Periodo',
  'Promedio',
  'Punt Promedio',
  'Estatus',
  'Punt Estatus',
  'Ingresos',
  'Punt Ingresos',
  'Modalidad',
  'Total',
];

const createXLSX = (students, institutes, { genders, periods, schoolGrades, stats, economicIncomes }, states, res) => {
  const wb = new xl.Workbook();
  const ws = wb.addWorksheet('Sheet 1');
  const bgStyle = wb.createStyle({
    fill: {
      type: 'pattern',
      patternType: 'solid',
      bgColor: '#72ad47',
      fgColor: '#72ad47',
    },
    font: {
      color: '#FFFFFF',
      size: 12,
    },
  });

  headers.forEach((header, index) => {
    ws.cell(1, index + 1)
      .string(header)
      .style(bgStyle);
  });

  students.forEach(({ curp, email, scholarshipRecord, personalData, addressData, scholarData, serial }, i) => {
    const address = `${addressData.street} ${addressData.number}`;
    const scholarshipStatus =
      scholarshipRecord[0]?.scholarship === scholarshipRecord[1]?.scholarship ? 'Renovación' : 'Nuevo ingreso';

    ws.cell(i + 2, 1).string(!!serial ? 'Completo' : 'Incompleto');
    ws.cell(i + 2, 2).string(scholarshipRecord[0]?.status);
    ws.cell(i + 2, 3).string(serial || '');
    ws.cell(i + 2, 4).string(personalData.name);
    ws.cell(i + 2, 5).string(personalData.fstSurname);
    ws.cell(i + 2, 6).string(personalData.sndSurname);
    ws.cell(i + 2, 7).string(personalData.gender);

    Object.entries(genders).forEach(([gender, points]) => {
      if (personalData.gender === gender) ws.cell(i + 2, 8).number(parseInt(points));
    });

    ws.cell(i + 2, 9).number(personalData.age);
    ws.cell(i + 2, 10).string(curp);
    ws.cell(i + 2, 11).string(address);
    ws.cell(i + 2, 12).string(addressData.postalCode);
    ws.cell(i + 2, 13).string(addressData.neighborhood);
    ws.cell(i + 2, 14).string(addressData.city);

    states.forEach(({ state, cities }) => {
      if (state === addressData.state)
        cities.forEach(({ city, points }) => {
          if (addressData.city === city) ws.cell(i + 2, 15).number(points);
        });
    });

    ws.cell(i + 2, 16).string(addressData.state);
    ws.cell(i + 2, 17).string(addressData.housePhoneNumber);
    ws.cell(i + 2, 18).string(addressData.mobilePhoneNumber);
    ws.cell(i + 2, 19).string(email);
    ws.cell(i + 2, 20).string(scholarData.institute);

    institutes.forEach(({ institute, careers, value: institute_value }) => {
      if (scholarData.institute === institute) {
        ws.cell(i + 2, 21).number(institute_value);
        careers.forEach(({ career, value: career_value }) => {
          if (scholarData.career === career) ws.cell(i + 2, 23).number(career_value);
        });
      }
    });

    ws.cell(i + 2, 22).string(scholarData.career);
    ws.cell(i + 2, 24).string(scholarData.semester);

    Object.entries(periods).forEach(([range, points]) => {
      const [min, max] = range.split('-');
      const semester = parseInt(scholarData.semester);
      if (min <= semester && semester <= max) ws.cell(i + 2, 25).number(parseInt(points));
    });

    ws.cell(i + 2, 26).string(scholarData.schoolGrade);

    Object.entries(schoolGrades).forEach(([range, points]) => {
      const [min, max] = range.split('-');
      const studentSchoolGrade = parseFloat(scholarData.schoolGrade);
      if (min <= studentSchoolGrade && studentSchoolGrade <= max) ws.cell(i + 2, 27).number(parseInt(points));
    });

    ws.cell(i + 2, 28).string(scholarshipStatus);

    Object.entries(stats).forEach(([status, points]) => {
      if (scholarshipStatus === status) {
        ws.cell(i + 2, 29).number(parseInt(points));
      }
    });

    ws.cell(i + 2, 30).string(`$${personalData?.economicIncomes || ''}`);

    Object.entries(economicIncomes).forEach(([range, points]) => {
      const [min, max] = range.split('-');
      const studentIncome = parseInt(personalData?.economicIncomes);
      if ((min <= studentIncome) & (studentIncome <= max)) ws.cell(i + 2, 31).number(parseInt(points));
    });

    ws.cell(i + 2, 32).string(scholarshipRecord[0].scholarship);
    ws.cell(i + 2, 33).formula(`H${i + 2}+O${i + 2}+U${i + 2}+W${i + 2}+Y${i + 2}+AA${i + 2}+AC${i + 2}+AE${i + 2}`);
  });

  wb.write('Concentrado.xlsx', res);
};

export { createXLSX };
