import { promisify } from 'util';
import { PDFDocument, rgb } from 'pdf-lib';
import pdf from 'html-pdf';
import fs from 'fs';

const table_content = (family, student_name) => {
  let tabledata = ``;
  family.forEach((member) => {
    tabledata += `<tr>`;
    for (let value in member) tabledata += `<td><center>${member[value]}</center></td>`;
    tabledata += `</tr>`;
  });

  return `
    <img src="https://cozcyt.gob.mx/wp-content/uploads/2018/07/cropped-COZCyT-2016-2021-2.png">
    <p>Describe los miembros de tu familia (inicia por madre/padre/tutor)</p>
    <table id="parents">
      <tr>
        <th><center>Nombre</center></th>
        <th><center>Parentesco</center></th>
        <th width='3%'><center>Edad</center></th>
        <th width='3%'><center>Sexo</center></th>
        <th><center>Estado Civil</center></th>
        <th><center>Escolaridad</center></th>
        <th width='7%'><center>Completa / Incompleta</center></th>
        <th><center>¿Trabaja?</center></th>
        <th><center>Ocupaci&oacute;n</center></th>
        <th><center>Lugar de trabajo</center></th>
        <th width='7%'><center>Ingreso mensual</center></th>
      </tr>
      ${tabledata}
    </table>
    <center>
      <p class="note">
        <span id="note"><b>NOTA: </b></span>
        El proporcionar informaci&oacute;n falsa es motivo suficiente para anular el tr&aacute;mite. El COZCyT se reserva el derecho de investigar la veracidad de lo antes declarado.
      </p>
    </center>
    <center>
      <p>${student_name}</p>
    </center>
    <hr class="line">
    <center>
      <p>Nombre del (a) Estudiante</p>
    </center>
    <center>
      <p>
        Manifiesto que la informaci&oacute;n proporcionada es ver&iacute;dica y de buena fe. De caso contrario me atendr&eacute; a las sanciones correspondientes.
      </p>
    </center>
    <style>
      th {
        font-size: 12px;
      }
      p {
        font-family: Arial, Helvetica, sans-serif;
        color: green;
      }
      img {
        margin: 0 20%;
        width: 60%;
      }
      #parents {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
      }
      #parents td, #parents th {
        border: 1px solid #ddd;
        padding: 0 3px;
      }
      #parents th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
      }
      #parents td {
        padding: 3px 0;
      }
      #note {
        font-family: Arial, Helvetica, sans-serif;
        color: red;
      }
      .note {
        padding: 2% 20%;
      }
      .line {
        width: 30%;
      }
    </style>
  `;
};
const table_buffer = async (family, student_name) => {
  const createdPDF = pdf.create(table_content(family, student_name), {
    orientation: 'landscape',
    format: 'A3',
    border: {
      top: '2cm',
      right: '2cm',
      bottom: '2cm',
      left: '2cm',
    },
  });
  const getBufferOfPDF = promisify(createdPDF.__proto__.toBuffer).bind(createdPDF);
  return await getBufferOfPDF();
};

const socioeconomicPDF = async (user_data, student_name, newPath) => {
  const {
    works,
    ocupacion,
    telefonoTrabajo,
    horario,
    sueldoMensual,
    viveCon,
    numeroRecamaras,
    tiempoViviendoEnEseDomicilio,
    personasViviendoEnLaCasa,
    laCasaDondeViveEs,
    materialDePiso,
    numeroBanios,
    tieneSala,
    tieneCocinaIndependiente,
    serviciosQueTiene,
    numeroCarros,
    cuentasConSeguroDeGastosMedicos,
    enCasaTienes,
    transporteQueUtilizas,
    family,
  } = user_data;

  const base_buffer = fs.readFileSync(`${__dirname}/socioeconomic.pdf`);
  const base = await PDFDocument.load(base_buffer);
  const table = await PDFDocument.load(await table_buffer(family, student_name));
  const final_document = await PDFDocument.create();
  const [basePage] = await final_document.copyPages(base, [0]);
  const [tablePage] = await final_document.copyPages(table, [0]);
  const basicParams = { size: 10, color: rgb(0.1, 0.1, 0.1) };

  basePage.drawText(works, { x: 125, y: 553, ...basicParams });
  basePage.drawText(ocupacion, { x: 210, y: 565, ...basicParams });
  basePage.drawText(telefonoTrabajo, { x: 435, y: 565, ...basicParams });
  basePage.drawText(horario, { x: 200, y: 542, ...basicParams });
  basePage.drawText(sueldoMensual, { x: 418, y: 542, ...basicParams });
  switch (viveCon) {
    case 'padres': {
      basePage.drawText('X', { x: 160, y: 514, ...basicParams });
      break;
    }
    case 'familiares': {
      basePage.drawText('X', { x: 224, y: 514, ...basicParams });
      break;
    }
    case 'amigos': {
      basePage.drawText('X', { x: 338, y: 515, ...basicParams });
      break;
    }
    case 'conyuge': {
      basePage.drawText('X', { x: 440, y: 515, ...basicParams });
      break;
    }
    default:
      break;
  }
  basePage.drawText(`${tiempoViviendoEnEseDomicilio}`, { x: 232, y: 483, ...basicParams });
  basePage.drawText(`${personasViviendoEnLaCasa}`, { x: 253, y: 452, ...basicParams });
  switch (laCasaDondeViveEs) {
    case 'propia': {
      basePage.drawText('X', { x: 177, y: 426, ...basicParams });
      break;
    }
    case 'rentada': {
      basePage.drawText('X', { x: 285, y: 426, ...basicParams });
      break;
    }
    case 'huespedes': {
      basePage.drawText('X', { x: 367, y: 426, ...basicParams });
      break;
    }
    default: {
      basePage.drawText('X', { x: 177, y: 408, ...basicParams });
      basePage.drawText(laCasaDondeViveEs, { x: 223, y: 408, ...basicParams });
      break;
    }
  }
  switch (materialDePiso) {
    case 'tierra': {
      basePage.drawText('X', { x: 177, y: 389, ...basicParams });
      break;
    }
    case 'madera': {
      basePage.drawText('X', { x: 285, y: 389, ...basicParams });
      break;
    }
    case 'cemento': {
      basePage.drawText('X', { x: 367, y: 389, ...basicParams });
      break;
    }
    case 'mosaico': {
      basePage.drawText('X', { x: 458, y: 390, ...basicParams });
      break;
    }
    case 'alfombra': {
      basePage.drawText('X', { x: 177, y: 371, ...basicParams });
      break;
    }
    case 'duela': {
      basePage.drawText('X', { x: 285, y: 371, ...basicParams });
      break;
    }
    default: {
      basePage.drawText('X', { x: 367, y: 371, ...basicParams });
      basePage.drawText(materialDePiso, { x: 415, y: 371, ...basicParams });
      break;
    }
  }
  basePage.drawText(`${numeroRecamaras}`, { x: 202, y: 352, ...basicParams });
  basePage.drawText(`${numeroBanios}`, { x: 418, y: 352, ...basicParams });
  basePage.drawText(tieneSala, { x: 139, y: 334, ...basicParams });
  basePage.drawText(tieneCocinaIndependiente, { x: 449, y: 334, ...basicParams });
  if (serviciosQueTiene.includes('agua')) basePage.drawText('X', { x: 175, y: 315, ...basicParams });
  if (serviciosQueTiene.includes('luz')) basePage.drawText('X', { x: 261, y: 315, ...basicParams });
  if (serviciosQueTiene.includes('drenaje')) basePage.drawText('X', { x: 358, y: 315, ...basicParams });
  if (serviciosQueTiene.includes('pavimento')) basePage.drawText('X', { x: 452, y: 315, ...basicParams });
  if (serviciosQueTiene.includes('telefono')) basePage.drawText('X', { x: 175, y: 297, ...basicParams });
  if (serviciosQueTiene.includes('gas')) basePage.drawText('X', { x: 261, y: 297, ...basicParams });
  if (serviciosQueTiene.includes('cable')) basePage.drawText('X', { x: 358, y: 297, ...basicParams });
  if (serviciosQueTiene.includes('internet')) basePage.drawText('X', { x: 452, y: 297, ...basicParams });
  if (enCasaTienes.includes('dvd')) basePage.drawText('X', { x: 176, y: 278, ...basicParams });
  if (enCasaTienes.includes('estufa')) basePage.drawText('X', { x: 261, y: 278, ...basicParams });
  if (enCasaTienes.includes('computadora')) basePage.drawText('X', { x: 358, y: 278, ...basicParams });
  if (enCasaTienes.includes('estereo')) basePage.drawText('X', { x: 452, y: 278, ...basicParams });
  if (enCasaTienes.includes('television')) basePage.drawText('X', { x: 176, y: 259, ...basicParams });
  if (enCasaTienes.includes('licuadora')) basePage.drawText('X', { x: 261, y: 259, ...basicParams });
  if (enCasaTienes.includes('lavadora')) basePage.drawText('X', { x: 358, y: 259, ...basicParams });
  if (enCasaTienes.includes('microondas')) basePage.drawText('X', { x: 452, y: 259, ...basicParams });

  basePage.drawText(`${numeroCarros}`, { x: 225, y: 240, ...basicParams });
  basePage.drawText(cuentasConSeguroDeGastosMedicos, { x: 270, y: 223, ...basicParams });
  switch (transporteQueUtilizas) {
    case 'autoPropio': {
      basePage.drawText('X', { x: 174, y: 203, ...basicParams });
      break;
    }
    case 'autoFamiliar': {
      basePage.drawText('X', { x: 261, y: 203, ...basicParams });
      break;
    }
    case 'motocicleta': {
      basePage.drawText('X', { x: 358, y: 203, ...basicParams });
      break;
    }
    case 'camion': {
      basePage.drawText('X', { x: 453, y: 203, ...basicParams });
      break;
    }
    case 'taxi': {
      basePage.drawText('X', { x: 174, y: 184, ...basicParams });
      break;
    }
    case 'caminando': {
      basePage.drawText('X', { x: 262, y: 184, ...basicParams });
      break;
    }
    default: {
      basePage.drawText('X', { x: 358, y: 184, ...basicParams });
      basePage.drawText(transporteQueUtilizas, { x: 404, y: 186, ...basicParams });
      break;
    }
  }

  final_document.addPage(basePage);
  final_document.addPage(tablePage);

  const final_buffer = await final_document.save();
  fs.writeFileSync(newPath, final_buffer);
};

export { socioeconomicPDF };
