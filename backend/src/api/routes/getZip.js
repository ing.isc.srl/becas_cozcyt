import { Router } from 'express';
import zip from 'express-zip';

import { authMiddleware } from '../middlewares';
import { StudentServices } from '../../services';
import { handleDocumentsData } from '../../utils';

const getZip = Router();

getZip.get('/:curp', authMiddleware, async (req, res) => {
  const { userType: isAdmin } = req.decoded;
  if (!isAdmin) {
    const { curp } = req.params;
    const { documents } = await StudentServices.getDocuments(curp);
    const files = handleDocumentsData(documents);
    res.zip(files, `${curp}.zip`);
  } else res.send({ message: 'You need be admin!' });
});

export { getZip };
