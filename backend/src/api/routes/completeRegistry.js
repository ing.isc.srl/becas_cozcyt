import { Router } from 'express';
import fs from 'fs';

import { decrypt } from '../../utils';
import { AccountService } from '../../services';

const completeRegistry = Router();

completeRegistry.get('/:curp', async (req, res) => {
  const encodedCurp = req.params.curp;
  const curp = decrypt(encodedCurp);
  try {
    await AccountService.completeSignUp(curp);
    try {
      fs.mkdirSync(`uploads/${curp}`);
    } catch {}
  } catch {}
  res.redirect('http://talentoscyt.zacatecas.gob.mx/');
});

export { completeRegistry };
