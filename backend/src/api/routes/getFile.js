import { Router } from 'express';

import { StudentServices } from '../../services';
import { authMiddleware } from '../middlewares';

const getFile = Router();

getFile.get('/:document', authMiddleware, async (req, res) => {
  const { curp } = req.decoded;
  const document = req.params.document;
  const { documents } = await StudentServices.fetchDocumentData(curp, document);
  res.download(documents[document].path);
});

getFile.get('/:curp/:document', authMiddleware, async (req, res) => {
  const { userType: isAdmin } = req.decoded;
  if (!isAdmin) {
    const { curp, document } = req.params;
    const { documents } = await StudentServices.fetchDocumentData(curp, document);
    res.download(documents[document].path);
  } else res.send('Nel');
});

export { getFile };
