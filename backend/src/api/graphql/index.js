import globalTypes from './globalTypes.gql';
import account from './account';
import institute from './institute';
import message from './message';
import scholarshipRequests from './scholarshipRequests';
import session from './session';
import scores from './scores';
import states from './states';
import student from './student';

const resolvers = {
  Query: {
    ...institute.resolvers.Query,
    ...message.resolvers.Query,
    ...scores.resolvers.Query,
    ...scholarshipRequests.resolvers.Query,
    ...states.resolvers.Query,
    ...student.resolvers.Query,
  },
  Mutation: {
    ...account.resolvers.Mutation,
    ...institute.resolvers.Mutation,
    ...message.resolvers.Mutation,
    ...scores.resolvers.Mutation,
    ...scholarshipRequests.resolvers.Mutation,
    ...session.resolvers.Mutation,
    ...student.resolvers.Mutation,
  },
  Subscription: {
    ...message.resolvers.Subscription,
    ...scholarshipRequests.resolvers.Subscription,
  },
};

const typeDefs = `
  ${globalTypes}
  ${account.typeDefs}
  ${institute.typeDefs}
  ${message.typeDefs}
  ${scores.typeDefs}
  ${scholarshipRequests.typeDefs}
  ${session.typeDefs}
  ${states.typeDefs}
  ${student.typeDefs}
`;

export { resolvers, typeDefs };
