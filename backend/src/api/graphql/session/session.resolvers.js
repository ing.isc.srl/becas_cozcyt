import { verifyToken } from '../../middlewares';
import { SessionService } from '../../../services';

const login = async (root, args, context) => {
  const { curp, password } = args;
  const response = await SessionService.Login(curp, password);
  return response;
};

const logout = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  await SessionService.Logout(context.token);
  return 'Bye Bye!';
};

export default {
  Mutation: {
    login,
    logout,
  },
};
