import resolvers from './session.resolvers';
import typeDefs from './session.gql';

// Done

export default {
  resolvers,
  typeDefs,
};
