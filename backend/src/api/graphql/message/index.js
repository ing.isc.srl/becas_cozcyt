import resolvers from './message.resolver';
import typeDefs from './message.gql';

export default {
  resolvers,
  typeDefs,
};
