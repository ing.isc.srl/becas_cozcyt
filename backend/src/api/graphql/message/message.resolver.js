import { PubSub, withFilter } from 'graphql-subscriptions';

import { verifyToken } from '../../middlewares';
import { MessageService } from '../../../services';

const pubsub = new PubSub();

const getAllMessages = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { curp } = args;
  const allMessages = await MessageService.fetchMessages(curp);
  return allMessages;
};

const sendMessage = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { curp, message } = args;
  const allMessages = await MessageService.appendMessage(curp, message);
  pubsub.publish('ALL_MESSAGES', { allMessages });
  return 'Message sent';
};

const markMessageDone = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { id: _id } = args;
  const { curp } = await verifyToken(context.token);
  const response = await MessageService.updateMessageStatus(curp, _id);
  return response;
};

const allMessages = {
  subscribe: withFilter(
    () => pubsub.asyncIterator(['ALL_MESSAGES']),
    async (payload, vars, context) => {
      const { curp: verifiedCurp } = await verifyToken(context.token);
      const { curp } = payload.allMessages;
      return curp === verifiedCurp;
    },
  ),
};

export default {
  Query: {
    getAllMessages,
  },
  Mutation: {
    sendMessage,
    markMessageDone,
  },
  Subscription: {
    allMessages,
  },
};
