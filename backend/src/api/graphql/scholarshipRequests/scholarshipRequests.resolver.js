import { PubSub } from 'graphql-subscriptions';

import { EmailService, ScholarshipRequestsServices } from '../../../services';
import { verifyToken } from '../../middlewares';
import { sortRequests, sortDataToGraphic } from '../../../utils';

const pubsub = new PubSub();

const getLastScholarship = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { curp } = args;
  const { scholarshipRecord } = await ScholarshipRequestsServices.getScholarships(curp);
  const [lastScholarship] = scholarshipRecord;
  return lastScholarship;
};

const getScholarshipsRequestActualPeriod = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { scholarship } = args;
  const students = await ScholarshipRequestsServices.getStudentsRequests(scholarship);
  const { allCompletedRequests } = sortRequests(students);
  const sortedRequests = [
    ...allCompletedRequests.filter(({ status }) => status === 'Pendiente'),
    ...allCompletedRequests.filter(({ status }) => status === 'Aprobada'),
    ...allCompletedRequests.filter(({ status }) => status === 'Rechazada'),
  ];
  return sortedRequests;
};

const getScholarshipsUnfinishedRequestActualPeriod = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { scholarship } = args;
  const students = await ScholarshipRequestsServices.getStudentsRequests(scholarship);
  const { allUnfinishedRequests } = sortRequests(students);
  return allUnfinishedRequests;
};

const getDataToGraphics = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { param: term } = args;
  const studentsData = await ScholarshipRequestsServices.fetchStudentData(term);
  const dataToGraphic = sortDataToGraphic(term, studentsData);
  return dataToGraphic;
};

const getLimitDate = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const limitDate = ScholarshipRequestsServices.fetchLimitDate();
  return limitDate;
};

const addScholarship = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { scholarship } = args;
  const { curp } = await verifyToken(context.token);
  const response = await ScholarshipRequestsServices.appendScholarship(curp, scholarship);
  return response;
};

const checkDocument = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { curp, document, status } = args;
  const response = ScholarshipRequestsServices.checkStudentDocument(curp, document, status);
  return response;
};

const sendListOfWrongFiles = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { curp } = args;
  const { email, personalData, documents } = await ScholarshipRequestsServices.fetchStudentDocuments(curp);
  EmailService.emailWithList(email, personalData, documents);
  return 'Mail sent!';
};

const checkScholarshipRequest = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { curp, status, type: scholarship } = args;
  const students = await ScholarshipRequestsServices.updateStudentsRequest(curp, status, scholarship);
  const { allCompletedRequests } = sortRequests(students);

  pubsub.publish('SCHOLARSHIP_REQUEST', {
    scholarshipRequests: [
      ...allCompletedRequests.filter(({ status }) => status === 'Pendiente'),
      ...allCompletedRequests.filter(({ status }) => status === 'Aprobada'),
      ...allCompletedRequests.filter(({ status }) => status === 'Rechazada'),
    ],
  });
  return `Status: ${status}`;
};

const establishLimitDate = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { date } = args;
  ScholarshipRequestsServices.updateLimitDate(date);
  return 'Date established!';
};

const scholarshipRequests = {
  subscribe: () => pubsub.asyncIterator(['SCHOLARSHIP_REQUEST']),
};

export default {
  Query: {
    getLastScholarship,
    getScholarshipsRequestActualPeriod,
    getScholarshipsUnfinishedRequestActualPeriod,
    getDataToGraphics,
    getLimitDate,
  },
  Mutation: {
    addScholarship,
    checkDocument,
    checkScholarshipRequest,
    sendListOfWrongFiles,
    establishLimitDate,
  },
  Subscription: {
    scholarshipRequests,
  },
};
