import resolvers from './scholarshipRequests.resolver';
import typeDefs from './scholarshipRequests.gql';

export default {
  resolvers,
  typeDefs,
};
