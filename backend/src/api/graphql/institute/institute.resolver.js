import { verifyToken } from '../../middlewares';
import { InstituteServices } from '../../../services';

const allInstitutes = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const institutes = await InstituteServices.fetchInstitutes();
  return institutes;
};

const allCareers = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { institute } = args;
  const { careers: careersOfInstitute } = await InstituteServices.fetchCareersOfInstitute(institute);
  const careers = careersOfInstitute.map(({ career, value }) => ({ key: career, value }));
  return careers;
};

const addInstitute = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { name: institute, value } = args;
  await InstituteServices.newInstitute(institute, value);
  return 'Institute added!';
};

const updateInstituteName = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { institute, newName } = args;
  await InstituteServices.renameInstitute(institute, newName);
  return 'Updated!';
};

const deleteInstitute = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { institute } = args;
  await InstituteServices.deleteInstitute(institute);
  return 'Deleted!';
};

const addCareer = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { institute, career, value } = args;
  await InstituteServices.addCareerToInstitute(institute, career, value);
  return 'Career added!';
};

const updateCareerName = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { institute, career, newName } = args;
  await InstituteServices.renameCareerOfInstitute(institute, career, newName);
  return 'Updated!';
};

const deleteCareer = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { institute, career } = args;
  await InstituteServices.deleteCareerOfInstitute(institute, career);
  return 'Deleted!';
};

export default {
  Query: {
    allInstitutes,
    allCareers,
  },
  Mutation: {
    addInstitute,
    updateInstituteName,
    deleteInstitute,
    addCareer,
    updateCareerName,
    deleteCareer,
  },
};
