import typeDefs from './institute.gql';
import resolvers from './institute.resolver';

export default {
  resolvers,
  typeDefs,
};
