import { verifyToken } from '../../middlewares';
import { AccountService, EmailService } from '../../../services';
import { decrypt } from '../../../utils';

const addAdmin = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { curp, email, password } = args;
  const response = await AccountService.addNewAdmin(curp, email, password);
  return response;
};

const addTemporalData = async (root, args, context) => {
  const { curp, email, password } = args;
  const success = await AccountService.signUp(curp, email, password);
  if (success) {
    EmailService.confirmationEmail(curp, email);
    return 'New student added!';
  } else return 'CURP o correo existente';
};

const changePassword = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { oldPassword, newPassword } = args;
  const { curp } = await verifyToken(context.token);
  const response = await AccountService.updatePassword(curp, oldPassword, newPassword);
  return response;
};

const recoverPasswordEmail = async (root, args, context) => {
  const { email, curp } = args;
  const user = await AccountService.findUserToSendRecoveryEmail(email, curp);
  if (user !== null) {
    EmailService.recoveryEmail(email, curp);
    return 'Mail sent!';
  } else return 'Usuario inexistente';
};

const establishNewPassword = async (root, args, context) => {
  const { curp: encoded, newPassword } = args;
  const { curp, exp } = JSON.parse(decrypt(encoded));
  if (new Date() < new Date(exp)) {
    await AccountService.recoverPassword(curp, newPassword);
    return 'changed';
  } else return 'Expired';
};

export default {
  Mutation: {
    addAdmin,
    addTemporalData,
    changePassword,
    recoverPasswordEmail,
    establishNewPassword,
  },
};
