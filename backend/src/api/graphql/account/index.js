import typeDefs from './account.gql';
import resolvers from './account.resolver';

export default {
  resolvers,
  typeDefs,
};
