import resolvers from './scores.resolvers';
import typeDefs from './scores.gql';

export default {
  resolvers,
  typeDefs,
};
