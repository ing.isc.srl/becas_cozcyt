import { verifyToken } from '../../middlewares';
import { ScoresService } from '../../../services';

const getPoints = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { param: term } = args;
  const listedPoints = await ScoresService.getListedScores(term);
  let arrayWithData = [];
  for (let key in listedPoints) arrayWithData.push({ key, value: listedPoints[key] });
  return arrayWithData;
};

const getValueCities = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { cities } = await ScoresService.getCitiesOfZacatecas();
  const { points } = cities.find(({ city }) => city !== 'Zacatecas' && city !== 'Guadalupe');
  const citiesData = cities
    .map(({ city: key, points: value }) => {
      if (key === 'Zacatecas' || key === 'Guadalupe') return { key, value };
    })
    .filter(Boolean);
  return [...citiesData, { key: 'Otros', value: points }];
};

const getValueInstitute = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { institute } = args;
  const { value } = await ScoresService.getValueOfInstitute(institute);
  return value;
};

const changeValueCities = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { city, value } = args;
  await ScoresService.updateCityValue(city, value);
  return 'changed!';
};

const changeValueInstitute = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { institute, value } = args;
  await ScoresService.updateInstituteValue(institute, value);
  return 'Changed!';
};

const changeValueCareer = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { institute, career, value } = args;
  await ScoresService.updateCareerValue(institute, career, value);
  return 'Changed!';
};

const changeValuePoints = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { field: hash, key: field, value } = args;
  ScoresService.updateListedScores(hash, field, value);
  return 'changed!';
};

export default {
  Query: {
    getPoints,
    getValueCities,
    getValueInstitute,
  },
  Mutation: {
    changeValuePoints,
    changeValueCities,
    changeValueInstitute,
    changeValueCareer,
  },
};
