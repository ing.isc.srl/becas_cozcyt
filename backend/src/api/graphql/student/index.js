import resolvers from './student.resolver';
import typeDefs from './student.gql';

export default {
  resolvers,
  typeDefs,
};
