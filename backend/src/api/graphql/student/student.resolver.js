import { verifyToken } from '../../middlewares';
import { StudentServices } from '../../../services';

const findStudent = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { curp } = args;
  const student = await StudentServices.fetchStudent(curp);
  return student;
};

const addUpdatePersonalData = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { curp } = await verifyToken(context.token);
  const { ...personalData } = args;
  await StudentServices.updatePersonalData(curp, personalData);
  return 'Data added!';
};

const addUpdateAddressData = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { curp } = await verifyToken(context.token);
  const { ...addressData } = args;
  await StudentServices.updateAddressData(curp, addressData);
  return 'Data added!';
};

const addUpdateScholarData = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { curp } = await verifyToken(context.token);
  const { ...scholarData } = args;
  await StudentServices.updateScholarData(curp, scholarData);
  return 'Data added!';
};

export default {
  Query: {
    findStudent,
  },
  Mutation: {
    addUpdatePersonalData,
    addUpdateAddressData,
    addUpdateScholarData,
  },
};
