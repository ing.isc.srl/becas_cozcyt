import resolvers from './states.resolvers';
import typeDefs from './states.gql';

export default {
  resolvers,
  typeDefs,
};
