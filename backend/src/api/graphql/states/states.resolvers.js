import { verifyToken } from '../../middlewares';
import { StatesServices } from '../../../services';

const allStates = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const states = await StatesServices.getStates();
  return states.map(({ state }) => state);
};

const allCities = async (root, args, context) => {
  if (!(await verifyToken(context.token))) return null;
  const { state } = args;
  const { cities } = await StatesServices.getCitiesOfSelectedState(state);
  return cities.map(({ city }) => city);
};

export default {
  Query: {
    allStates,
    allCities,
  },
};
