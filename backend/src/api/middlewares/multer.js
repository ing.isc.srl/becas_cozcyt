import multer from 'multer';

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const { curp } = req.decoded;
    cb(null, `./uploads/${curp}`);
  },
  filename: (req, file, cb) => {
    cb(null, `${file.originalname}`);
  },
});

export const upload = multer({ dest: './uploads/', storage });
