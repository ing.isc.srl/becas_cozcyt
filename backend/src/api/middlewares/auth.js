import '@babel/polyfill';
import { Router } from 'express';
import jwt from 'jsonwebtoken';
import fs from 'fs';

import { BlacklistJWT } from '../../models';

const SECRET_KEY = fs.readFileSync('private.key');
const authMiddleware = Router();

// For resolvers
const verifyToken = async (token) => {
  if (!!token) {
    return await jwt.verify(token, SECRET_KEY, async (err, decoded) => {
      if (!!(await BlacklistJWT.findOne({ token }))) return false;
      if (!!err) return false;
      return decoded;
    });
  } else return false;
};

// For routes
authMiddleware.use(async (req, res, next) => {
  const decoded = await verifyToken(req.headers.authorization);
  if (!!decoded) {
    req.decoded = decoded;
    next();
  } else res.send({ message: 'Invalid JWT' });
});

export { verifyToken, authMiddleware };
