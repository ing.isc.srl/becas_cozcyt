import mongoose from 'mongoose';
import redis from 'redis';

export const connectMongo = () => {
  mongoose.connect('mongodb://localhost/becas_cozcyt', {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  });

  mongoose.connection.on('error', console.error.bind(console, 'connection error:'));
  mongoose.connection.once('open', () => {
    console.log('🚀 Mongo client connected!');
  });
};

export const redisClient = redis.createClient({ password: 'invierno' });
redisClient.on('connect', () => {
  console.log('🚀 Redis client connected!');
});
