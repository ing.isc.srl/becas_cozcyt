import { ApolloServer } from 'apollo-server-express';
import bodyParser from 'body-parser';
import express from 'express';
import http from 'http';
import cors from 'cors';

import { resolvers, typeDefs } from './api/graphql';
import { router } from './api/routes';
import { resetDataAtDate } from './utils';
import { connectMongo } from './db';

const HOST = 'localhost';
const PORT = 3000;
const app = express();

connectMongo();

app.use(cors());
app.use(bodyParser.json()); // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // to support URL-encoded bodies
app.use(router);

resetDataAtDate();

const server = new ApolloServer({
  typeDefs,
  resolvers,
  playground: false,
  introspection: false,
  subscriptions: {
    onConnect: (connectionParams, webSocket, context) => {
      const token = connectionParams['Authorization'];
      return { token };
    },
  },
  context: async ({ req, connection }) => {
    if (connection) {
      return connection.context;
    } else {
      try {
        const token = req.headers.authorization;
        return { token };
      } catch {
        return { token: '' };
      }
    }
  },
});

server.applyMiddleware({ app });

const httpServer = http.createServer(app);
server.installSubscriptionHandlers(httpServer);

httpServer.listen({ port: PORT, host: HOST }, () => {
  console.log(`🚀 Server ready at http://${HOST}:${PORT}${server.graphqlPath}`);
  console.log(`🚀 Subscriptions ready at ws://${HOST}:${PORT}${server.subscriptionsPath}`);
});
