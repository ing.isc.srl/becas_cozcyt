import AccountService from './account';
import EmailService from './email';
import InstituteServices from './institute';
import MessageService from './message';
import ScoresService from './scores';
import ScholarshipRequestsServices from './scholarshipRequests';
import SessionService from './session';
import StatesServices from './states';
import StudentServices from './student';

export {
  AccountService,
  EmailService,
  InstituteServices,
  MessageService,
  ScoresService,
  ScholarshipRequestsServices,
  SessionService,
  StatesServices,
  StudentServices,
};
