import { promisify } from 'util';

import { States, Institutes } from '../models';
import { redisClient } from '../db';

const rHGetAll = promisify(redisClient.hgetall).bind(redisClient);

const getListedScores = async (hash) => rHGetAll(hash);

const getCitiesOfZacatecas = async () => States.findOne({ state: 'Zacatecas' }, { _id: 0, cities: 1 });

const getValueOfInstitute = async (institute) => Institutes.findOne({ institute }, { _id: 0, value: 1 });

const updateListedScores = (hash, field, value) => {
  redisClient.hset(hash, field, value);
};

const updateInstituteValue = async (institute, value) => {
  await Institutes.findOneAndUpdate({ institute }, { value });
};

const updateCareerValue = async (institute, career, value) => {
  await Institutes.findOneAndUpdate(
    {
      institute,
      careers: {
        $elemMatch: { career },
      },
    },
    {
      'careers.$.value': value,
    },
  );
  return 'Changed!';
};

const updateCityValue = async (city, value) => {
  if (city === 'Zacatecas' || city === 'Guadalupe')
    await States.findOneAndUpdate(
      {
        state: 'Zacatecas',
        cities: {
          $elemMatch: {
            city,
          },
        },
      },
      {
        'cities.$.points': value,
      },
    );
  else
    await States.findOneAndUpdate(
      { state: 'Zacatecas' },
      {
        $set: {
          'cities.$[elem].points': value,
        },
      },
      {
        arrayFilters: [
          {
            $and: [
              {
                'elem.city': {
                  $ne: 'Zacatecas',
                },
              },
              {
                'elem.city': {
                  $ne: 'Guadalupe',
                },
              },
            ],
          },
        ],
      },
    );
};

export default {
  getListedScores,
  getValueOfInstitute,
  getCitiesOfZacatecas,
  updateInstituteValue,
  updateCareerValue,
  updateListedScores,
  updateCityValue,
};
