import send from '../smtp';
import { encrypt } from '../utils';

const confirmationEmail = (curp, email) => {
  const encrypted = encrypt(curp);
  const message = (Header, Paragraph, Button) => `
    <div style="width: 500px; margin: 0 auto;">
      <div style="display: flex;">
        <img style="width: 500px" src="https://cozcyt.gob.mx/wp-content/uploads/2018/07/cropped-COZCyT-2016-2021-2.png"/>
      </div>
      <b style="${Header}">¡Hola!</b>
      <p style="${Paragraph}">Por favor pulsa el siguiente botón para confirmar tu correo electrónico.</p>
      <a style="${Button}" href="https://talentoscyt.zacatecas.gob.mx/api/completeRegistry/${encrypted}">
        Confirmar correo electrónico
      </a>
      <p style="${Paragraph}">Si no has creado ninguna cuenta, puedes ignorar o eliminar este e-mail.</p>
      <p style="${Paragraph}">Saludos,<br/>COZCyT</p>
    </div>
  `;
  send(email, 'Confirmar correo electrónico', message);
};

const recoveryEmail = (email, curp) => {
  const dt = new Date();
  dt.setHours(dt.getHours() + 1);
  const exp = dt.toLocaleString();
  const encrypted = encrypt(JSON.stringify({ curp, exp }));

  const message = (Header, Paragraph, Button) => `
    <div style="width: 500px; margin: 0 auto;">
      <div style="display: flex;">
        <img style="width: 500px" src="https://cozcyt.gob.mx/wp-content/uploads/2018/07/cropped-COZCyT-2016-2021-2.png"/>
      </div>
      <b style="${Header}">¡Hola!</b>
      <p style="${Paragraph}">Por favor pulsa el siguiente botón para continuar con el proceso de reestablecer tu contraseña.</p>
      <a style="${Button}" href="https://talentoscyt.zacatecas.gob.mx/#/actualizar_clave/${encrypted}">
        Reestablecer contraseña
      </a>
      <p style="${Paragraph}">Saludos,<br/>COZCyT</p>
    </div>
  `;
  send(email, 'Recuperación de contraseña', message);
};

const emailWithList = (email, personalData, documents) => {
  const message = (Header, Paragraph, Button) => {
    const { name, fstSurname, sndSurname, gender } = personalData;
    const greetings = gender === 'Hombre' ? 'Estimado alumno' : 'Estimado alumna';
    const wrongFiles = [
      { doc: 'curp', name: 'CURP' },
      { doc: 'birthAct', name: 'Acta de nacimiento' },
      { doc: 'ine', name: 'INE' },
      { doc: 'photoHouse', name: 'Foto de la casa' },
      { doc: 'reasonLetter', name: 'Carta motivos' },
      { doc: 'socioeconomicStudy', name: 'Estudio socioeconomico' },
      { doc: 'economicIncomes', name: 'Comprobante de ingresos' },
      { doc: 'studyConstancy', name: 'Constancia de estudios' },
      { doc: 'curriculumVitae', name: 'Curriculum Vitae' },
      { doc: 'qualificationConstancy', name: 'Kardex de calificaciones' },
      { doc: 'acknowledgment', name: 'Reconocimiento' },
      { doc: 'socialService', name: 'Constancia de servicio social' },
    ]
      .map(({ doc, name }) => {
        if (documents[doc]?.status === 'Rechazado') return `<li style="${Header}">${name}</li>`;
      })
      .filter(Boolean)
      .join('');

    return `
      <div style="width: 500px; margin: 0 auto;">
        <div style="display: flex;">
          <img style="width: 500px" src="https://cozcyt.gob.mx/wp-content/uploads/2018/07/cropped-COZCyT-2016-2021-2.png"/>
        </div>   
        <b style="${Header}">${greetings} ${name} ${fstSurname} ${sndSurname}</b>
        <p style="${Paragraph}">
          Estos son los documentos que no cumplen con los requisitos en el proceso de solicitud.
        </p>
        <ul>
          ${wrongFiles}
        </ul>
      </div>
    `;
  };

  send(email, 'Estatus de Solicitud', message);
  send('becas@cozcyt.gob.mx', 'Estatus de Solicitud', message);
};

export default {
  confirmationEmail,
  recoveryEmail,
  emailWithList,
};
