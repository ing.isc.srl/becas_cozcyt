import { Institutes } from '../models';

const fetchInstitutes = async () => Institutes.find({}, { _id: 0, careers: 0 });

const fetchCareersOfInstitute = async (institute) => await Institutes.findOne({ institute }, { _id: 0, careers: 1 });

const newInstitute = async (institute, value) => {
  await new Institutes({ institute, value }).save();
};

const renameInstitute = async (institute, newName) => {
  await Institutes.findOneAndUpdate({ institute }, { institute: newName });
};

const deleteInstitute = async (institute) => {
  await Institutes.findOneAndRemove({ institute });
};

const addCareerToInstitute = async (institute, career, value) => {
  await Institutes.findOneAndUpdate(
    { institute },
    {
      $push: {
        careers: { career, value },
      },
    },
  );
};

const renameCareerOfInstitute = async (institute, career, newName) => {
  await Institutes.findOneAndUpdate(
    {
      institute,
      careers: {
        $elemMatch: { career },
      },
    },
    {
      'careers.$.career': newName,
    },
  );
};

const deleteCareerOfInstitute = async (institute, career) => {
  await Institutes.findOneAndUpdate(
    { institute },
    {
      $pull: {
        careers: { career },
      },
    },
  );
};

export default {
  fetchInstitutes,
  fetchCareersOfInstitute,
  newInstitute,
  renameInstitute,
  deleteInstitute,
  addCareerToInstitute,
  renameCareerOfInstitute,
  deleteCareerOfInstitute,
};
