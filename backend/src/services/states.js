import { States } from '../models';

const getStates = async () => States.find({}, { _id: 0, state: 1 });

const getCitiesOfSelectedState = async (state) => States.findOne({ state }, { _id: 0, cities: 1 });

export default {
  getStates,
  getCitiesOfSelectedState,
};
