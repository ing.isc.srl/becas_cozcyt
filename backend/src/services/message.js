import { Student, Message } from '../models';

const fetchMessages = async (curp) => Message.findOne({ curp }, { _id: 0 });

const appendMessage = async (curp, message) => {
  const user = await Student.findOne({ curp }, { _id: 1 });
  let allMessages;
  if (user !== null) {
    const newMessage = { message, sendDate: new Date() };
    try {
      allMessages = await Message.findOneAndUpdate({ curp }, {});
      allMessages.messages = [newMessage, ...allMessages.messages];
      await allMessages.save();
    } catch (err) {
      allMessages = await new Message({ curp, messages: [newMessage] }).save();
    }
  }
  return allMessages;
};

const updateMessageStatus = async (curp, _id) => {
  const filter = { curp, messages: { $elemMatch: { _id } } };
  const projection = { _id: 0, 'messages.$.done': 1 };
  try {
    const { messages } = await Message.findOne(filter, projection);
    const [{ done }] = messages;
    await Message.findOneAndUpdate(filter, { 'messages.$.done': !done });
    return 'Done';
  } catch (e) {
    return "Message doesn't exists";
  }
};

export default {
  fetchMessages,
  appendMessage,
  updateMessageStatus,
};
