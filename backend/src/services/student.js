import { Student } from '../models';

const fetchStudent = async (curp) => Student.findOne({ curp }, { _id: 0 });

const updatePersonalData = async (curp, personalData) => {
  const updatedData = {};
  Object.entries(personalData).forEach(([key, value]) =>
    Object.assign(updatedData, {
      [`personalData.${key}`]: value,
    }),
  );
  await Student.findOneAndUpdate({ curp }, { $set: updatedData });
};

const updateAddressData = async (curp, addressData) => {
  await Student.findOneAndUpdate({ curp }, { addressData });
};

const updateScholarData = async (curp, scholarData) => {
  await Student.findOneAndUpdate({ curp }, { scholarData });
};

const fetchDocumentData = async (curp, document) =>
  Student.findOne(
    { curp },
    {
      [`documents.${document}.path`]: 1,
    },
  );

const getDocuments = async (curp) =>
  Student.findOne(
    { curp },
    {
      _id: 0,
      documents: 1,
    },
  );

const updateDataOfStudent = async (curp, document_path, economicIncomes) =>
  Student.findOneAndUpdate(
    { curp },
    {
      $set: {
        'documents.socioeconomicStudy': {
          path: document_path,
          status: 'Pendiente',
        },
        'personalData.economicIncomes': economicIncomes,
      },
    },
  );

const updateDocumentsData = async (curp, documents) => {
  await Student.findOneAndUpdate({ curp }, { documents });
};

export default {
  fetchStudent,
  updatePersonalData,
  updateAddressData,
  updateScholarData,
  fetchDocumentData,
  getDocuments,
  updateDataOfStudent,
  updateDocumentsData,
};
