import { promisify } from 'util';

import { actualPeriod } from '../utils';
import { Student, States, Institutes } from '../models';
import { redisClient } from '../db';

const rGet = promisify(redisClient.get).bind(redisClient);
const rHGetAll = promisify(redisClient.hgetall).bind(redisClient);

const fetchLimitDate = async () => rGet('limitDate');

const getScholarships = async (curp) =>
  Student.findOne(
    { curp },
    {
      _id: 0,
      scholarshipRecord: 1,
    },
  );

const getStudentsRequests = async (scholarship) =>
  Student.find(
    {
      'scholarshipRecord.0.period': actualPeriod(),
      'scholarshipRecord.0.scholarship': scholarship,
      documents: { $exists: true },
    },
    {
      _id: 0,
      curp: 1,
      scholarshipRecord: 1,
      serial: 1,
      personalData: 1,
      'scholarData.schoolGrade': 1,
      documents: 1,
    },
  );

const fetchStudentData = async (term) => {
  const subdocument = term === 'gender' ? 'personalData' : term === 'city' ? 'addressData' : 'scholarData';
  const projection =
    term === 'city'
      ? {
          [`${subdocument}.${term}`]: 1,
          [`${subdocument}.state`]: 1,
        }
      : {
          [`${subdocument}.${term}`]: 1,
        };
  return Student.find(
    {
      'scholarshipRecord.0.period': actualPeriod(),
      'scholarshipRecord.0.status': 'Aprobada',
      [subdocument]: {
        $exists: true,
      },
    },
    {
      _id: 0,
      ...projection,
    },
  );
};

const appendScholarship = async (curp, scholarship) => {
  const student = await Student.findOne({ curp }, { _id: 0, scholarshipRecord: 1 });
  const count = await rGet('serial');
  const number = '000'.substr(0, 4 - count.length) + count;

  if (student.scholarshipRecord[0]?.period === actualPeriod()) {
    return 'Ya solicitaste una beca prro';
  } else {
    redisClient.set('serial', parseInt(count) + 1);
    await Student.findOneAndUpdate(
      { curp },
      {
        serial: `${curp.substr(0, 4)}${number}`,
        $push: {
          scholarshipRecord: {
            $each: [
              {
                scholarship,
                period: actualPeriod(),
                registrationDate: new Date(),
              },
            ],
            $position: 0,
          },
        },
      },
    );
    return 'Data added!';
  }
};

const checkStudentDocument = async (curp, document, status) => {
  try {
    await Student.findOneAndUpdate(
      { curp },
      {
        [`documents.${document}.status`]: status,
      },
    );
    return 'Documento revisado!';
  } catch (e) {
    return 'Status no valido';
  }
};

const updateLimitDate = (date) => {
  redisClient.set('limitDate', date);
};

const updateStudentsRequest = async (curp, status, scholarship) => {
  await Student.findOneAndUpdate(
    {
      curp,
      'scholarshipRecord.0.period': actualPeriod(),
    },
    {
      'scholarshipRecord.0.status': status,
    },
  );
  return Student.find(
    {
      'scholarshipRecord.0.period': actualPeriod(),
      'scholarshipRecord.0.scholarship': scholarship,
      documents: { $exists: true },
    },
    {
      _id: 0,
      curp: 1,
      scholarshipRecord: 1,
      serial: 1,
      personalData: 1,
      'scholarData.schoolGrade': 1,
      documents: 1,
    },
  );
};

const fetchStudentDocuments = async (curp) =>
  Student.findOne(
    { curp },
    {
      _id: 0,
      email: 1,
      personalData: 1,
      documents: 1,
    },
  );

const getDataForDocument = async () => ({
  states: await States.find({}, { _id: 0 }),
  students: await Student.find(
    {
      'scholarshipRecord.0.period': actualPeriod(),
      documents: {
        $exists: true,
      },
    },
    {
      _id: 0,
      curp: 1,
      email: 1,
      personalData: 1,
      addressData: 1,
      scholarData: 1,
      scholarshipRecord: 1,
      serial: 1,
    },
  ),
  institutes: await Institutes.find(
    {},
    {
      _id: 0,
      institute: 1,
      value: 1,
      careers: 1,
    },
  ),
  points: {
    genders: await rHGetAll('gender'),
    periods: await rHGetAll('period'),
    schoolGrades: await rHGetAll('schoolGrades'),
    stats: await rHGetAll('status'),
    economicIncomes: await rHGetAll('economicIncomes'),
  },
});

export default {
  getScholarships,
  getStudentsRequests,
  fetchStudentData,
  fetchLimitDate,
  appendScholarship,
  checkStudentDocument,
  updateLimitDate,
  updateStudentsRequest,
  fetchStudentDocuments,
  getDataForDocument,
};
