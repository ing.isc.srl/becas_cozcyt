import bcrypt from 'bcrypt';

import { Admin, Student, TemporalData } from '../models';

const addNewAdmin = async (curp, email, passwordToHash) => {
  const password = await bcrypt.hash(passwordToHash, 10);
  try {
    await new Admin({ curp, email, password }).save();
    return 'New admin added!';
  } catch (e) {
    return 'CURP o correo existente';
  }
};

const signUp = async (curp, email, passwordToHash) => {
  const filter = { $or: [{ curp }, { email }] };
  const existingStudent = await Student.find(filter, { _id: 1 });
  const existingAdmin = await Admin.find(filter, { _id: 1 });

  if (existingAdmin.length === 0 && existingStudent.length === 0) {
    const password = await bcrypt.hash(passwordToHash, 10);
    try {
      await new TemporalData({ curp, email, password }).save();
      return true;
    } catch (err) {
      return false;
    }
  } else return false;
};

const completeSignUp = async (curp) => {
  const { email, password } = await TemporalData.findOne(
    { curp },
    {
      _id: 0,
      email: 1,
      password: 1,
    },
  );
  await TemporalData.deleteOne({ curp });
  await new Student({ curp, email, password }).save();
};

const updatePassword = async (curp, oldPassword, newPassword) => {
  const user =
    (await Admin.findOne({ curp }, { _id: 0, password: 1 })) ||
    (await Student.findOne({ curp }, { _id: 0, password: 1 }));

  if (!!user) {
    const isEqual = await bcrypt.compare(oldPassword, user.password);
    if (isEqual && oldPassword !== newPassword) {
      const password = await bcrypt.hash(newPassword, 10);

      if (user.collection.name === 'admins') {
        await Admin.findOneAndUpdate({ curp }, { password });
      } else await Student.findOneAndUpdate({ curp }, { password });

      return 'Password changed!';
    } else if (oldPassword === newPassword) {
      return 'La nueva clave no puede ser igual a la anterior';
    } else return 'Clave incorrecta';
  }
};

const findUserToSendRecoveryEmail = async (email, curp) => Student.findOne({ email, curp }, { _id: 1 });

const recoverPassword = async (curp, newPassword) => {
  const password = await bcrypt.hash(newPassword, 10);
  await Student.findOneAndUpdate({ curp }, { password });
};

export default {
  addNewAdmin,
  signUp,
  completeSignUp,
  updatePassword,
  findUserToSendRecoveryEmail,
  recoverPassword,
};
