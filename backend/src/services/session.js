import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import fs from 'fs';

import { Admin, Student, BlacklistJWT } from '../models';

const SECRET_KEY = fs.readFileSync('private.key');

const createToken = async (payload) =>
  jwt.sign(payload, SECRET_KEY, {
    expiresIn: '24h',
  });

const Login = async (curp, password) => {
  const user =
    (await Admin.findOne({ curp }, { _id: 0, password: 1 })) ||
    (await Student.findOne({ curp }, { _id: 0, password: 1 }));
  if (!!user) {
    const isEqual = await bcrypt.compare(password, user.password);
    const userType = user.collection.name === 'admins' ? 0 : 1;
    return isEqual ? createToken({ curp, userType }) : 'Clave incorrecta';
  } else return 'Usuario inexistente';
};

const Logout = async (token) => {
  await new BlacklistJWT({ token }).save();
};

export default {
  Login,
  Logout,
};
