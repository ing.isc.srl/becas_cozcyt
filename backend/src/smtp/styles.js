const common = `
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol';
  box-sizing: border-box;
  margin-top: 20px;
  margin-bottom: 20px;
`;

const Paragraph = `
  ${common}
  color: #3d4852;
  font-size: 16px;
  line-height: 1.5em;
  text-align: left;
`;

const Header = `
  ${common}
  color: #3d4852;
  font-size: 19px;
  font-weight: bold;
  text-align: left;
`;

const Button = `
  ${common}
  border-radius: 3px;
  color: #fff;
  font-size: 13px;
  text-align: center;
  width: 220px;
  margin-right: 140px;
  margin-left: 140px;
  display: inline-block;
  text-decoration: none;
  background-color: #3490dc;
  border-top: 10px solid #3490dc;
  border-right: 18px solid #3490dc;
  border-bottom: 10px solid #3490dc;
  border-left: 18px solid #3490dc;
`;

export { Paragraph, Header, Button };
