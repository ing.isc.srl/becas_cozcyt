import { Header, Paragraph, Button } from './styles';
import nodemailer from 'nodemailer';

const send = async (email, subject, message) => {
  const cozcytHost = true
    ? {
        host: 'cozcyt.gob.mx',
        user: 'becas@cozcyt.gob.mx',
        pass: 'Ap3+Di=Rk1(G',
      }
    : {
        host: 'correo.zacatecas.gob.mx',
        user: 'becas.cozcyt@zacatecas.gob.mx',
        pass: 'Becas.2020',
      };

  const transporter = nodemailer.createTransport({
    host: cozcytHost.host,
    port: 465,
    secure: true,
    auth: { user: cozcytHost.user, pass: cozcytHost.pass },
  });

  await transporter.sendMail({
    from: cozcytHost.user,
    to: email,
    subject,
    text: subject,
    html: message(Header, Paragraph, Button),
  });
};

export default send;
