import mongoose from 'mongoose';

export const careersSchema = mongoose.Schema({
  career: {
    type: String,
  },
  value: {
    type: Number,
    default: 0,
  },
});
