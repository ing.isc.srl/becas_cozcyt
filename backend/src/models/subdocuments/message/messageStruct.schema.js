import mongoose from 'mongoose';

export const messageStructSchema = mongoose.Schema({
  message: {
    type: String,
    required: true,
  },
  done: {
    type: Boolean,
    required: true,
    default: false,
  },
  sendDate: {
    type: Date,
    default: new Date(),
    required: true,
    expires: 3600 * 24 * 30 * 3,
  },
});
