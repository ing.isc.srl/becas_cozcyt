import mongoose from 'mongoose';

export const citySchema = mongoose.Schema({
  city: String,
  points: Number,
});
