import mongoose from 'mongoose';

const documentData = mongoose.Schema(
  {
    path: {
      type: String,
    },
    status: {
      type: String,
      enum: ['', 'Pendiente', 'Aprobado', 'Rechazado'],
    },
  },
  { _id: false },
);

export const documentsSchema = mongoose.Schema({
  curp: documentData,
  birthAct: documentData,
  ine: documentData,
  photoHouse: documentData,
  reasonLetter: documentData,
  socioeconomicStudy: documentData,
  economicIncomes: documentData,
  studyConstancy: documentData,
  curriculumVitae: documentData,
  qualificationConstancy: documentData,
  acknowledgment: documentData,
  socialService: documentData,
});
