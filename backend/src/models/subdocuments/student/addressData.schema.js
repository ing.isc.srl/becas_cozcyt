import mongoose from 'mongoose';

export const addressDataSchema = mongoose.Schema({
  street: String,
  number: String,
  postalCode: String,
  neighborhood: String,
  state: String,
  city: String,
  mobilePhoneNumber: String,
  housePhoneNumber: String,
});
