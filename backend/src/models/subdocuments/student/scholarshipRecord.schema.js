import mongoose from 'mongoose';

export const scholarshipRecordSchema = mongoose.Schema({
  scholarship: {
    type: String,
    enum: ['Talento especial', 'Excelencia académica', 'Mejora académica', 'Labsol'],
  },
  period: {
    type: String,
  },
  registrationDate: {
    type: Date,
    default: new Date(),
    required: true,
  },
  status: {
    type: String,
    default: 'Pendiente',
  },
});
