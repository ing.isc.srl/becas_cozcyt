export * from './personalData.schema';
export * from './addressData.schema';
export * from './scholarData.schema';
export * from './document.schema';
export * from './scholarshipRecord.schema';
