import mongoose from 'mongoose';

export const scholarDataSchema = mongoose.Schema({
  institute: String,
  career: String,
  semester: String,
  schoolGrade: String, // promedio
});
