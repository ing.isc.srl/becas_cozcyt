import mongoose from 'mongoose';

export const personalDataSchema = mongoose.Schema({
  name: String,
  fstSurname: String,
  sndSurname: String,
  age: Number,
  economicIncomes: String,
  gender: {
    type: String,
    enum: ['Hombre', 'Mujer'],
  },
  ethnicGroup: Boolean,
});
