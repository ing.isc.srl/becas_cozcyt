import mongoose from 'mongoose';

const blacklistJWTSchema = mongoose.Schema({
  token: String,
  expireDate: {
    type: Date,
    default: new Date(),
    expires: 3600 * 24,
  },
});

export const BlacklistJWT = mongoose.model('BlacklistJWTs', blacklistJWTSchema);
