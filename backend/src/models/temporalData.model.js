import mongoose from 'mongoose';

const temporalDataSchema = mongoose.Schema({
  curp: {
    type: String,
    required: true,
    unique: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
  },
  creationDate: {
    type: Date,
    expires: 3600 * 24,
    default: new Date(),
  },
});

export const TemporalData = mongoose.model('TemporalDatas', temporalDataSchema);
