import mongoose from 'mongoose';
import {
  personalDataSchema,
  addressDataSchema,
  scholarDataSchema,
  documentsSchema,
  scholarshipRecordSchema,
} from './subdocuments/student';

const studentSchema = mongoose.Schema({
  curp: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
  },
  serial: {
    type: String,
  },
  personalData: personalDataSchema,
  addressData: addressDataSchema,
  scholarData: scholarDataSchema,
  documents: documentsSchema,
  scholarshipRecord: [scholarshipRecordSchema],
});

export const Student = mongoose.model('Students', studentSchema);
