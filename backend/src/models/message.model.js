import mongoose from 'mongoose';
import { messageStructSchema } from './subdocuments/message/messageStruct.schema';

const messagesSchema = mongoose.Schema({
  curp: {
    type: String,
    required: true,
  },
  messages: [messageStructSchema],
});

export const Message = mongoose.model('Messages', messagesSchema);
