import mongoose from 'mongoose';
import { citySchema } from './subdocuments/state/cities.schema';

const stateSchema = mongoose.Schema({
  state: String,
  cities: [citySchema],
});

export const States = mongoose.model('States', stateSchema);
