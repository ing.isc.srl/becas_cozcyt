import mongoose from 'mongoose';

const adminSchema = mongoose.Schema({
  curp: {
    type: String,
    required: true,
    unique: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
  },
});

export const Admin = mongoose.model('Admins', adminSchema);
