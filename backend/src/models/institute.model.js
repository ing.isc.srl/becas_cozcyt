import mongoose from 'mongoose';
import { careersSchema } from './subdocuments/institute/careers.schema';

const institutesSchema = mongoose.Schema({
  institute: {
    type: String,
    unique: true,
  },
  value: {
    type: Number,
    default: 0,
  },
  careers: [careersSchema],
});

export const Institutes = mongoose.model('Institutes', institutesSchema);
