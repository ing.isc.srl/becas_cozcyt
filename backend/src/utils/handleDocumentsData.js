export const handleDocumentsData = (documents) => {
  delete documents._doc._id;
  return Object.entries(documents._doc).map(([doc, { path }]) => {
    const name = path.split('/')[2];
    return {
      path,
      name,
    };
  });
};
