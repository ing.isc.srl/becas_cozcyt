import CryptoJS from 'crypto-js';
import fs from 'fs';

const SECRET_KEY = fs.readFileSync('private.key').toString('utf-8');

export const encrypt = (text) => {
  const encrypted = CryptoJS.AES.encrypt(text, SECRET_KEY);
  return encrypted.toString().replace(/\//g, '_');
};

export const decrypt = (text) => {
  const bytes = CryptoJS.AES.decrypt(text.replace(/_/g, '/'), SECRET_KEY);
  return bytes.toString(CryptoJS.enc.Utf8);
};
