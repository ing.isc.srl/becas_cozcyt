export const actualPeriod = () => {
  const year = new Date().getFullYear();
  const period = new Date().getMonth() >= 6 ? `Agosto - Diciembre ${year}` : `Enero - Junio ${year}`;
  return period;
};
