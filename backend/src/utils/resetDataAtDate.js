import { redisClient } from '../db';
import { Student } from '../models';

const clock = require('date-events')();

export const resetDataAtDate = () => {
  clock.on('*:*', async (date) => {
    if ((date.getMonth() === 0 || date.getMonth() === 6) && date.getDate() === 1) {
      redisClient.set('serial', 1);
      await Student.updateMany({}, { $set: { serial: '' } });
    }
    clock.removeAllListeners();
  });
};
