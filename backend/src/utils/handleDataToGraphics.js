export const sortDataToGraphic = (term, studentsData) => {
  const subdocument = term === 'gender' ? 'personalData' : term === 'city' ? 'addressData' : 'scholarData';
  let statistics = [];

  switch (term) {
    case 'city': {
      const citiesOfZac = studentsData
        .map(({ addressData }) => {
          if (addressData.state === 'Zacatecas') return addressData.city;
        })
        .filter(Boolean);
      statistics = [...new Set(citiesOfZac)].map((city) => ({
        key: city,
        value: 0,
      }));
      statistics.push({
        key: 'Otros',
        value: 0,
      });
      break;
    }
    case 'gender': {
      statistics = [
        {
          key: 'Hombre',
          value: 0,
        },
        {
          key: 'Mujer',
          value: 0,
        },
      ];
      break;
    }
    default: {
      const keys = studentsData.map(({ scholarData }) => scholarData[term]);
      statistics = [...new Set(keys)].map((key) => ({
        key,
        value: 0,
      }));
      break;
    }
  }

  studentsData.forEach((student) => {
    statistics.forEach((field) => {
      if (term === 'city') {
        const { state, city } = student.addressData;
        if (state !== 'Zacatecas' && field.key === 'Otros') field.value += 1;
        else if (city === field.key) field.value += 1;
      } else if (student[subdocument][term] === field.key) field.value += 1;
    });
  });

  return statistics;
};
