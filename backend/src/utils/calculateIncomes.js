// Calculate monthly economic incomes
export const calculateIncomes = (sueldoMensual, family) => {
  let economicIncomes = 0;

  if (parseInt(sueldoMensual) > 0) economicIncomes += parseInt(sueldoMensual);
  family.forEach(({ ingreso }) => {
    if (parseInt(ingreso) > 0) economicIncomes += parseInt(ingreso);
  });

  return economicIncomes;
};
