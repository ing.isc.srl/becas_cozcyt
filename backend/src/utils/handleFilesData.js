export const handleFilesData = (files) => {
  const status = 'Pendiente';
  let documents = {};
  if (files.length > 5) {
    documents = {
      birthAct: {
        path: `${files[0].path}`,
        status,
      },
      ine: {
        path: `${files[1].path}`,
        status,
      },
      curp: {
        path: `${files[2].path}`,
        status,
      },
      reasonLetter: {
        path: `${files[3].path}`,
        status,
      },
      curriculumVitae: {
        path: `${files[4].path}`,
        status,
      },
      qualificationConstancy: {
        path: `${files[5].path}`,
        status,
      },
      studyConstancy: {
        path: `${files[6].path}`,
        status,
      },
      economicIncomes: {
        path: `${files[7].path}`,
        status,
      },
      photoHouse: {
        path: `${files[8].path}`,
        status,
      },
    };

    if (!!files[9]?.path)
      documents.acknowledgment = {
        path: `${files[9].path}`,
        status,
      };
  } else {
    documents = {
      ine: {
        path: `${files[0].path}`,
        status,
      },
      qualificationConstancy: {
        path: `${files[1].path}`,
        status,
      },
      studyConstancy: {
        path: `${files[2].path}`,
        status,
      },
      socialService: {
        path: `${files[3].path}`,
        status,
      },
    };

    if (!!files[4]?.path)
      documents.acknowledgment = {
        path: `${files[4].path}`,
        status,
      };
  }
  return documents;
};
