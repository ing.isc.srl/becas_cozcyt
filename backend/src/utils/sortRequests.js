const newRequestDocs = [
  'curp',
  'birthAct',
  'ine',
  'photoHouse',
  'reasonLetter',
  'socioeconomicStudy',
  'economicIncomes',
  'studyConstancy',
  'curriculumVitae',
  'qualificationConstancy',
];

const renovationDocs = ['ine', 'socioeconomicStudy', 'studyConstancy', 'qualificationConstancy', 'socialService'];

export const sortRequests = (students) => {
  let allUnfinishedRequests = [];
  let allCompletedRequests = [];

  for (let { scholarshipRecord, curp, serial, personalData, scholarData, documents } of students) {
    const { scholarship, status } = scholarshipRecord[0];
    const name = `${personalData?.name} ${personalData?.fstSurname} ${personalData?.sndSurname}`;
    const promedio = scholarData?.schoolGrade;
    const type = scholarship === scholarshipRecord[1]?.scholarship ? 'Renovación' : 'Nuevo Ingreso';
    let docsToCheck = [];
    let lacksDocuments = false;
    let allDocumentsApproved = true;

    switch (scholarship) {
      case 'Talento especial': {
        if (type === 'Nuevo Ingreso') docsToCheck.push(...newRequestDocs, 'acknowledgment');
        else docsToCheck.push(...renovationDocs, 'acknowledgment');
        break;
      }
      default: {
        if (type === 'Nuevo Ingreso') docsToCheck.push(...newRequestDocs);
        else docsToCheck.push(...renovationDocs);
        break;
      }
    }

    for (let doc of docsToCheck) {
      if (!documents[doc]?.status) {
        lacksDocuments = true;
        allDocumentsApproved = false;
        break;
      } else if (documents[doc].status !== 'Aprobado') {
        allDocumentsApproved = false;
      }
    }

    const requestData = {
      curp,
      serial,
      name,
      promedio,
      scholarship,
      status,
      allDocumentsApproved,
      type,
    };
    if (lacksDocuments) allUnfinishedRequests.push(requestData);
    else allCompletedRequests.push(requestData);
  }

  return {
    allCompletedRequests,
    allUnfinishedRequests,
  };
};
