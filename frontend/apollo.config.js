module.exports = {
	client: {
		service: {
			endpoint: {
				skipSSLValidation: true
			},
			name: "becas-cozcyt"
		},
		// Files processed by the extension
		includes: ["src/**/*.vue", "src/**/*.js"]
	}
};
