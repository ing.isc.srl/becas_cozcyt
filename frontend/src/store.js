import Vue from "vue";
import Vuex from "vuex";
import gql from "graphql-tag";
import { apolloClient } from "./graphql/apollo";
import EventBus from "./bus";
import router from "./router";
import { VueEasyJwt } from "vue-easy-jwt";
const jwt = new VueEasyJwt();

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		userData: {
			curp: "",
			type: 0
		},
		complete: null,
		messages: 0,
		limitDay: 1
	},
	mutations: {
		saveUserData(state) {
			const token = jwt.decodeToken(sessionStorage.getItem("token"));
			state.userData.curp = token.curp;
			state.userData.type = token.userType;
		},
		changeStepper(state, value) {
			state.complete = value;
		},
		messages(state, data) {
			state.messages = data;
		},
		modifyMessages(state) {
			state.messages--;
		},
		setLimitDay(state, data) {
			state.limitDay = data;
		}
	},
	actions: {
		async login({ commit, state }, user) {
			try {
				const token = await apolloClient.mutate({
					mutation: gql`
						mutation($curp: String!, $password: String!) {
							login(curp: $curp, password: $password)
						}
					`,
					variables: {
						curp: user.curp,
						password: user.pass
					}
				});
				if (
					token.data.login !== "Usuario inexistente" &&
					token.data.login !== "Clave incorrecta"
				) {
					sessionStorage.setItem("token", token.data.login);
					commit("saveUserData");
					if (state.userData.type === 1) {
						router.push({ name: "AvailableScholarships" });
					} else if (state.userData.type === 0) {
						router.push({ name: "Requests" });
					}
				} else {
					EventBus.$emit("errorLogin", token.data.login);
				}
			} catch (error) {}
		},
		async logout() {
			try {
				await apolloClient.mutate({
					mutation: gql`
						mutation {
							logout
						}
					`
				});
				sessionStorage.removeItem("token");
				router.push("/");
			} catch (error) {
				console.error(error);
			}
		},
		async prevRegister({}, user) {
			try {
				const { data } = await apolloClient.mutate({
					mutation: gql`
						mutation(
							$email: String!
							$curp: String!
							$password: String!
						) {
							addTemporalData(
								email: $email
								curp: $curp
								password: $password
							)
						}
					`,
					variables: {
						email: user.email,
						curp: user.curp,
						password: user.pass
					}
				});
				EventBus.$emit("responsePrevRegister", data.addTemporalData);
			} catch (error) {}
		},
		async savePersonalData({ commit, state }, user) {
			commit("saveUserData");
			try {
				await apolloClient.mutate({
					mutation: gql`
						mutation(
							$curp: String!
							$name: String!
							$fstSurname: String!
							$sndSurname: String!
							$gender: String!
							$ethnicGroup: Boolean!
							$age: Int!
						) {
							addUpdatePersonalData(
								curp: $curp
								name: $name
								fstSurname: $fstSurname
								sndSurname: $sndSurname
								gender: $gender
								ethnicGroup: $ethnicGroup
								age: $age
							)
						}
					`,
					variables: {
						curp: state.userData.curp,
						name: user.name,
						fstSurname: user.fstSurname,
						sndSurname: user.sndSurname,
						gender: user.gender,
						ethnicGroup: user.ethnicGroup,
						age: user.age
					}
				});
				EventBus.$emit("personalDataSaved");
			} catch (error) {}
		},
		async saveAddressData({ commit, state }, user) {
			commit("saveUserData");
			try {
				await apolloClient.mutate({
					mutation: gql`
						mutation(
							$curp: String!
							$state: String!
							$city: String!
							$neighborhood: String!
							$street: String!
							$number: String!
							$postalCode: String!
							$mobilePhoneNumber: String!
							$housePhoneNumber: String!
						) {
							addUpdateAddressData(
								curp: $curp
								state: $state
								city: $city
								neighborhood: $neighborhood
								street: $street
								number: $number
								postalCode: $postalCode
								mobilePhoneNumber: $mobilePhoneNumber
								housePhoneNumber: $housePhoneNumber
							)
						}
					`,
					variables: {
						curp: state.userData.curp,
						state: user.state,
						city: user.city,
						neighborhood: user.neighborhood,
						street: user.street,
						number: user.number,
						postalCode: user.postalCode,
						mobilePhoneNumber: user.mobilePhoneNumber,
						housePhoneNumber: user.housePhoneNumber
					}
				});
				EventBus.$emit("addressDataSaved");
			} catch (error) {}
		},
		async saveScholarData({ commit, state }, user) {
			commit("saveUserData");
			try {
				await apolloClient.mutate({
					mutation: gql`
						mutation(
							$curp: String!
							$institute: String!
							$career: String!
							$semester: String!
							$schoolGrade: String!
						) {
							addUpdateScholarData(
								curp: $curp
								institute: $institute
								career: $career
								semester: $semester
								schoolGrade: $schoolGrade
							)
						}
					`,
					variables: {
						curp: state.userData.curp,
						institute: user.institute,
						career: user.career,
						semester: user.semester,
						schoolGrade: user.schoolGrade
					}
				});
				EventBus.$emit("scholarDataSaved");
			} catch (error) {}
		},
		async addAdmin({}, admin) {
			try {
				const { data } = await apolloClient.mutate({
					mutation: gql`
						mutation(
							$curp: String!
							$email: String!
							$password: String!
						) {
							addAdmin(
								curp: $curp
								email: $email
								password: $password
							)
						}
					`,
					variables: {
						curp: admin.curp,
						email: admin.email,
						password: admin.pass
					}
				});
				EventBus.$emit("responseNewAdmin", data.addAdmin);
			} catch (error) {}
		},
		async getMessages({ commit }) {
			try {
				const { data } = await apolloClient.query({
					query: gql`
						query {
							getAllMessages {
								messages {
									message
								}
							}
						}
					`
				});
				commit("messages", data.getAllMessages.messages.length);
			} catch (error) {}
		},
		async actionRecoverPassword({}, user) {
			try {
				const { data } = await apolloClient.mutate({
					mutation: gql`
						mutation($curp: String!, $email: String!){
							recoverPasswordEmail(curp: $curp, email: $email)
						}
					`,
					variables: {
						curp: user.curp,
						email: user.email
					}
				});
				EventBus.$emit("responseRecoverPassword", data.recoverPasswordEmail);
			} catch(err) { }
		},
		async newPassword({}, user) {
			try {
				await apolloClient.mutate({
					mutation: gql`
						mutation($curp: String!, $newPassword: String!){
							establishNewPassword(curp: $curp, newPassword: $newPassword)
						}
					`,
					variables: {
						curp: user.curp,
						newPassword: user.pass
					}
				});
				EventBus.$emit("succNewPassword");
			} catch (error) { }
		},
		async deleteInstitute({}, institute){
			try {
				await apolloClient.mutate({
					mutation: gql`
						mutation($institute: String!){
							deleteInstitute(institute: $institute)
						}
					`,
					variables: {
						institute
					}
				});
				// EventBus.$emit("confirmDeleteInstitute", institute);
			} catch (error) { }
		},
		async changeNameInstitute({}, data){
			try {
				await apolloClient.mutate({
					mutation: gql`
						mutation($institute: String!, $newName: String!){
							updateInstituteName(institute: $institute, newName: $newName)
						}
					`,
					variables: {
						institute: data.institute,
						newName: data.newName
					}
				});
				EventBus.$emit("succChangeNameInstitute");
			} catch (error) { }
		},
		async changeNameCareer({}, data){
			try {
				await apolloClient.mutate({
					mutation: gql`
						mutation($institute: String!, $career: String!, $newName: String!){
							updateCareerName(institute: $institute, career: $career, newName: $newName)
						}
					`,
					variables:{
						institute: data.institute,
						career: data.career,
						newName: data.newName
					}
				});
				EventBus.$emit("succChangeNameCareer", data.institute);
			} catch (error) { }
		},
		async deleteCareer({}, data){
			try {
				await apolloClient.mutate({
					mutation: gql`
						mutation($institute: String!, $career: String!){
							deleteCareer(institute: $institute, career: $career)
						}
					`,
					variables:{
						institute: data.institute,
						career: data.career
					}
				});
				EventBus.$emit("succDeleteCareer",data.institute);
			} catch (error) { }
		}
	}
});
