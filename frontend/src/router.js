import Vue from "vue";
import Router from "vue-router";
import { isAuth, isAdmin, isStudent } from "./auth";

Vue.use(Router);

const router = new Router({
	mode: "hash",
	base: process.env.BASE_URL,
	routes: [
		{
			path: "/",
			name: "Login",
			component: () => import("./views/Login.vue")
		},
		{
			path: "/perfil",
			name: "Profile",
			component: () => import("./views/student/Profile.vue"),
			meta: {
				requiresAuth: true,
				requireStudent: true
			}
		},
		{
			path: "/convocatorias",
			name: "AvailableScholarships",
			component: () => import("./views/student/AvailableScholarships.vue"),
			meta: {
				requiresAuth: true,
				requireStudent: true
			}
		},
		{
			path: "/solicitudes-de-beca",
			name: "ScolarshipRequest",
			component: () => import("./views/student/ScholarshipRequest.vue"),
			meta: {
				requiresAuth: true,
				requireStudent: true
			}
		},
		{
			path: "/mensajes",
			name: "Notifications",
			component: () => import("./views/student/Notifications.vue"),
			meta: {
				requiresAuth: true,
				requireStudent: true
			}
		},
		{
			path: "/convocatorias/nuevo-ingreso/:typeNewScholarship",
			name: "NewScholarship",
			component: () => import("./views/student/NewScholarship.vue"),
			meta: {
				requiresAuth: true,
				requireStudent: true
			}
		},
		{
			path: "/convocatorias/renovación/:typeRenovationScholarship",
			name: "RenovationScholarship",
			component: () =>
				import("./views/student/RenovationScholarship.vue"),
			meta: {
				requiresAuth: true,
				requireStudent: true
			}
		},
		{
			path: "/convocatorias/:typeScholarship/mis-documentos",
			name: "MyDocuments",
			component: () => import("./views/student/MyDocuments.vue"),
			meta: {
				requiresAuth: true,
				requireStudent: true
			}
		},
		{
			path: "/actualizar_clave/:key",
			name: "UpdatePassword",
			component: () => import("./views/student/UpdatePassword.vue")
		},
		{
			path: "/solicitudes",
			name: "Requests",
			component: () => import("./views/admin/Requests.vue"),
			meta: {
				requiresAuth: true,
				requireAdmin: true
			}
		},
		{
			path: "/estudiante/:student/información",
			name: "InfoStudent",
			component: () => import("./views/admin/InfoStudent.vue"),
			meta: {
				requiresAuth: true,
				requireAdmin: true
			}
		},
		{
			path: "/estudiante/:student/documentos/:typeScholarshipAdmin",
			name: "DocumentsStudent",
			component: () => import("./views/admin/DocumentsStudent.vue"),
			meta: {
				requiresAuth: true,
				requireAdmin: true
			}
		},
		{
			path: "/estadísticas",
			name: "Statistics",
			component: () => import("./views/admin/Statistics.vue"),
			meta: {
				requiresAuth: true,
				requireAdmin: true
			}
		},
		{
			path: "/puntajes",
			name: "Scores",
			component: () => import("./views/admin/Scores.vue"),
			meta: {
				requiresAuth: true,
				requireAdmin: true
			}
		},
		{
			path: "/estudiante/:student/mensajes",
			name: "MessagesStudent",
			component: () => import("./views/admin/MessagesStudent.vue"),
			meta: {
				requiresAuth: true,
				requireAdmin: true
			}
		},
		{
			path: "*",
			redirect: "/"
		}
	]
});

router.beforeEach((to, from, next) => {
	to.matched.some(route => {
		// Requiere autenticacion de cualquier usuario
		if (route.meta.requiresAuth) {
			if (!isAuth()) {
				next({ path: "/login" });
			}
		}
		// Requiere autenticacion de estudiante
		if (route.meta.requireStudent) {
			if (!isStudent()) {
				next({ name: "Login" });
			}
		}
		// Requiere autenticacion de administrados
		if (route.meta.requireAdmin) {
			if (!isAdmin()) {
				next({ name: "Login" });
			}
		}
		next();
	});
});

export default router;
