import { VueEasyJwt } from "vue-easy-jwt";
const jwt = new VueEasyJwt();

function isAuth() {
	try {
		let token = jwt.decodeToken(sessionStorage.getItem("token"));
		return jwt.isExpired(token);
	} catch (error) {
		return false;
	}
}

function isAdmin() {
	let token = jwt.decodeToken(sessionStorage.getItem("token"));
	return token.userType === 0;
}

function isStudent() {
	let token = jwt.decodeToken(sessionStorage.getItem("token"));
	return token.userType === 1;
}

export { isAuth, isAdmin, isStudent };
