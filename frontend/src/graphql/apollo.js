import { ApolloClient } from "apollo-client";
import { createHttpLink } from "apollo-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";
import { setContext } from "apollo-link-context";
import { split } from "apollo-link";
import { WebSocketLink } from "apollo-link-ws";
import { getMainDefinition } from "apollo-utilities";

// HTTP connection to the API
const httpLink = createHttpLink({
	// You should use an absolute URL here
	// uri: "http://192.168.1.71:3000/graphql"
	// uri: "http://localhost:3000/graphql"
	uri: "https://talentoscyt.zacatecas.gob.mx/graphql"
});

const wsLink = new WebSocketLink({
	// uri: "ws://192.168.1.71:3000/graphql",
	// uri: "ws://localhost:3000/graphql",
	uri: "wss://talentoscyt.zacatecas.gob.mx/graphql",
	options: {
		reconnect: true
	}
});

const link = split(
	// split based on operation type
	({ query }) => {
		const definition = getMainDefinition(query);
		return (
			definition.kind === "OperationDefinition" &&
			definition.operation === "subscription"
		);
	},
	wsLink,
	httpLink
);

const authMiddleware = setContext((_, { headers }) => {
	// get the authentication token from local storage if it exists
	const token = sessionStorage.getItem("token");
	// return the headers to the context so httpLink can read them
	return {
		headers: {
			...headers,
			authorization: token ? `${token}` : "No autorizado"
		}
	};
});

const defaultOptions = {
	watchQuery: {
		fetchPolicy: "no-cache",
		errorPolicy: "ignore"
	},
	query: {
		fetchPolicy: "no-cache",
		errorPolicy: "all"
	}
};

// Cache implementation
const cache = new InMemoryCache();
// Create the apollo client
export const apolloClient = new ApolloClient({
	link: authMiddleware.concat(link),
	cache,
	defaultOptions: defaultOptions,
	connectToDevTools: true
});
