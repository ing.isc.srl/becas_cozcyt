import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "@/plugins/vuetify";
import VueApollo from "vue-apollo";
import { apolloClient } from "../src/graphql/apollo";
import axios from "axios";
import VueAxios from "vue-axios";

Vue.use(VueAxios, axios);
Vue.use(VueApollo);

// axios.defaults.baseURL = "http://192.168.1.71:3000/api";
axios.defaults.baseURL = "http://localhost:3000/api";
// axios.defaults.baseURL = "https://talentoscyt.zacatecas.gob.mx/api";
axios.defaults.headers.common[
	"Authorization"
] = `Bearer ${sessionStorage.getItem("token")}`;

const apolloProvider = new VueApollo({
	defaultClient: apolloClient
});

Vue.config.productionTip = false;

new Vue({
	router,
	store,
	vuetify,
	apolloProvider,
	render: h => h(App)
}).$mount("#app");
